module gitlab.com/joobus/ckjv-bible/html_gen

go 1.14

require (
	github.com/cbroglie/mustache v1.0.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/sergi/go-diff v1.1.0
)
